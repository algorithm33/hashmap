/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.woramet.hashmap;

/**
 *
 * @author User
 */
public class TestHashMap {
    public static void main(String[] args) {
        HashMap h1 = new HashMap(15, "Woramet"); // เพิ่มข้อมูล
        h1.put(20, "Phatra");
        h1.put(40, "Bas");
        
        System.out.println(h1.get(15));
         System.out.println(h1.get(20));
          System.out.println(h1.get(40));
          
          for(int i = 0 ; i < 10 ; i ++){
            System.out.println(h1.get(i)); // โชว์ข้อมูลทั้งหมด
        }

    }
}
