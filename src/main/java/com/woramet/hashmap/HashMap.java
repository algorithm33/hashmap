/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.woramet.hashmap;

/**
 *
 * @author User
 */
public class HashMap {
        private int key;
        private String value;
        private String [ ] HashMapTable = new String [ 15 ]; //กำหนดขนาด = 15

    public HashMap(int key, String value) {
        this.key = key;
        this.value = value;
         HashMapTable[hash(key)]=value;
    }
    
    public int hash (int key){
        return key%HashMapTable.length;
    }
    
    public void put (int key, String value){
        HashMapTable[hash(key)] = value;
    }
    
    public String get (int key){
        return (String) HashMapTable[hash(key)];
    }
}
